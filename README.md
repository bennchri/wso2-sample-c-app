# Demo projects for WSO2 C-app feature 

You have three projects in total. They are built using  `Eclipse Java EE IDE for Web Developers.
Version: Juno Service Release 2`.
With `WSO2 Developer Studio plugins version 3.2.0 added`

# helloworld

Contains a webapp displaying a hello world message. The project is built using `Developer Studio Dashboard | Web Application`

# StockQuote
Contains a Axis2 web service (SimpleStockQuote). The project is built using `Developer Studio Dashboard | Axis2 Service Project`

# SampleCarbonApp
This project is the project that builds the c-app it is built using `Developer Studio Dashboard | Carbon Application Project`. This is a "to be safe" project you should be able to demo the creation instead

# Demo Suggestions

* I usually have the helloworld and StockQuote projects presetup in a workspace and do the first demo by illustrating the "Dashboard Content" going over the different sections and explaining them. 
* After that I demo the creation of a c-app by creating a new `Carbon Application Project` via Menu:Developer Studio | Open Dashboard | Carbon Application Project
** Give the project a name 
** Choose artifacts that should be included in the c-app. Observe that it will show all WSO2 artifacts available in the workspace. Also observe that it will only show artifacts built as WSO2 projects (either imported as WSO2 project or via the Dashboard)
** Open the pom with the specific "Carbon Application Project POM Editor" if not done by default. In "Dependencies" you will now see the artifacts and their respective server role. Here you can assign a different server role if necessary
** When done export the c-app artifact (car-file) by right clicking the Carbon Application project and choose "Export Carbon Application Project" et voila

# Setup procedure
* Clone
* Import one at a time into Dev studio using "Import | WSO2 | Existing WSO2 Project"



